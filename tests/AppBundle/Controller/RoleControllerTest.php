<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use AppBundle\DataFixtures\ORM\Fixtures;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;

class RoleControllerTest extends WebTestCase
{
    protected function setUp() {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $this->em = static::$kernel->getContainer()
        ->get( 'doctrine' )
        ->getManager();

        $loader = new Loader();
        $loader->addFixture( new Fixtures() );

        $purger = new ORMPurger( $this->em );
        $executor = new ORMExecutor( $this->em, $purger );
        $purger->purge();
    }

    protected function populateDB() {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $this->em = static::$kernel->getContainer()
        ->get( 'doctrine' )
        ->getManager();

        $loader = new Loader();
        $loader->addFixture( new Fixtures() );

        $purger = new ORMPurger( $this->em );
        $executor = new ORMExecutor( $this->em, $purger );
        $executor->execute( $loader->getFixtures() );
    }

    public function testAllRoles() {
        $this->setUp();
        $client = static::createClient();

        $container = self::$kernel->getContainer();
        $em = $container->get('doctrine')->getManager();

        $response = $client->request('GET', '/role');
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
        $this->assertFalse( $client->getResponse()->headers->contains(
            'Content-Type',
            'application/json'
        ) );
    }

    public function testOneRoleGoodId() {
        $client = static::createClient();

        $container = self::$kernel->getContainer();
        $em = $container->get('doctrine')->getManager();

        $response = $client->request('GET', '/role/1');
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
        $this->assertFalse( $client->getResponse()->headers->contains(
            'Content-Type',
            'application/json'
        ) );
    }

    public function testOneRoleBadId() {
        $client = static::createClient();

        $container = self::$kernel->getContainer();
        $em = $container->get('doctrine')->getManager();

        $response = $client->request('GET', '/role/f');
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
        $this->assertFalse( $client->getResponse()->headers->contains(
            'Content-Type',
            'application/json'
        ) );
    }

    public function testAllRolesWithDB() {
        $this->populateDB();
        $client = static::createClient();

        $container = self::$kernel->getContainer();

        $response = $client->request('GET', '/role');
        $this->assertEquals( 3, count( json_decode( $client->getResponse()->getContent() ) ) );
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertTrue( $client->getResponse()->headers->contains(
            'Content-Type',
            'application/json'
        ) );
    }

}
