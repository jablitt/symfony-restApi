<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use AppBundle\DataFixtures\ORM\Fixtures;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;

class UserControllerTest extends WebTestCase
{
    protected function setUp() {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $this->em = static::$kernel->getContainer()
        ->get( 'doctrine' )
        ->getManager();

        $loader = new Loader();
        $loader->addFixture( new Fixtures() );

        $purger = new ORMPurger( $this->em );
        $executor = new ORMExecutor( $this->em, $purger );
        $purger->purge();
    }

    protected function populateDB() {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $this->em = static::$kernel->getContainer()
        ->get( 'doctrine' )
        ->getManager();

        $loader = new Loader();
        $loader->addFixture( new Fixtures() );

        $purger = new ORMPurger( $this->em );
        $executor = new ORMExecutor( $this->em, $purger );
        $executor->execute( $loader->getFixtures() );
    }

    public function testAllUsers() {
        $this->setUp();
        $client = static::createClient();

        $container = self::$kernel->getContainer();

        $response = $client->request('GET', '/user');
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
        $this->assertFalse( $client->getResponse()->headers->contains(
            'Content-Type',
            'application/json'
        ) );
    }

    public function testOneUserGoodId() {
        $client = static::createClient();

        $container = self::$kernel->getContainer();

        $response = $client->request('GET', '/user/1');
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
        $this->assertFalse( $client->getResponse()->headers->contains(
            'Content-Type',
            'application/json'
        ) );
    }

    public function testOneUserBadId() {
        $client = static::createClient();

        $container = self::$kernel->getContainer();

        $response = $client->request('GET', '/user/f');
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
        $this->assertFalse( $client->getResponse()->headers->contains(
            'Content-Type',
            'application/json'
        ) );
    }

    public function testAllUsersWithDB() {
        $this->populateDB();
        $client = static::createClient();

        $container = self::$kernel->getContainer();

        $response = $client->request('GET', '/user');
        $this->assertEquals( 3, count( json_decode( $client->getResponse()->getContent() ) ) );
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertTrue( $client->getResponse()->headers->contains(
            'Content-Type',
            'application/json'
        ) );
    }

    public function testCreateUser() {
        $this->populateDB();
        $client = static::createClient();

        $container = self::$kernel->getContainer();

        $response = $client->request('POST', '/user', array( 'user' =>
        array( 'firstname' => 'Josh',
                'lastname' => 'Ablitt',
                'email' => '2@2.com',
                'password' => 'test',
                'role' => 1
        ) ) );
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertFalse( $client->getResponse()->headers->contains(
            'Content-Type',
            'application/json'
        ) );
    }

}
