<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testUserForm()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/userForm');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}
