##Software Versions
* Vagrant 2.0.0
* VirtualBox 5.0.4
* Composer 1.5.2
* MacOS 10.12.6

##Setup
* Composer Install
* Update line 10 of homestead.yaml to current project directory
* vagrant up
* vagrant ssh
* cd /var/home/code
* php bin/console doctrine:database:create
* php bin/console doctrine:schema:update --force
* to load some sample data - php bin/console doctrine:fixtures:load
* Navigate to http://192.168.10.10/

##Tests
* navigate to project root
* phpunit

I had issues getting consistent ID's from the database, so I was unable to test any routes requiring an ID.
