<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* Retailer
*
* @ORM\Table(name="retailer")
* @ORM\Entity(repositoryClass="AppBundle\Repository\RetailerRepository")
*/
class Retailer
{
    /**
    * @var int
    *
    * @ORM\Column(name="id", type="integer")
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    private $id;

    /**
    * @var string
    *
    * @ORM\Column(name="name", type="string", length=255)
    */
    private $name;

    /**
    * @var string
    *
    * @ORM\Column(name="location", type="string", length=255)
    */
    private $location;

    /**
    * @var string
    *
    * @ORM\Column(name="email", type="string", length=255)
    */
    private $email;

    /**
    * @var string
    *
    * @ORM\Column(name="secret", type="string", length=255)
    */
    private $secret;

    /**
    * @ORM\OneToMany(targetEntity="Orders", mappedBy="id")
    */
    protected $orders;


    /**
    * Get id
    *
    * @return int
    */
    public function getId()
    {
        return $this->id;
    }

    /**
    * Set name
    *
    * @param string $name
    *
    * @return Retailer
    */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
    * Get name
    *
    * @return string
    */
    public function getName()
    {
        return $this->name;
    }

    /**
    * Set location
    *
    * @param string $location
    *
    * @return Retailer
    */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
    * Get location
    *
    * @return string
    */
    public function getLocation()
    {
        return $this->location;
    }

    /**
    * Set email
    *
    * @param string $email
    *
    * @return Retailer
    */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
    * Get email
    *
    * @return string
    */
    public function getEmail()
    {
        return $this->email;
    }

    /**
    * Set secret
    *
    * @param string $secret
    *
    * @return Retailer
    */
    public function setSecret($secret)
    {
        $this->secret = $secret;

        return $this;
    }

    /**
    * Get secret
    *
    * @return string
    */
    public function getSecret()
    {
        return $this->secret;
    }

    /**
    *  Override to string, allows use in select on user form
    * @return string name
    */
    public function __toString()
    {
        return $this->name;
    }
}
