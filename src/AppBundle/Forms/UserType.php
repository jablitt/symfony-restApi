<?php

namespace AppBundle\Forms;

use AppBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType {

  public function buildForm( FormBuilderInterface $builder, array $options ) {

    $builder->add( 'firstname', TextType::class )
    ->add( 'lastname', TextType::class )
    ->add( 'email', TextType::class )
    ->add( 'password', TextType::class )
    ->add( 'role' )
    ->add( 'Submit', SubmitType::class, array( 'label' => 'Submit' ) );
  }

  public function configureOptions( OptionsResolver $resolver ) {
    $resolver->setDefaults( array(
            'data_class' => User::class,
        ));
  }

}
