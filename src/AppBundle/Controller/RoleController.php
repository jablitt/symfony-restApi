<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use FOS\RestBundle\View\View;
use AppBundle\Entity\Role;

class RoleController extends FOSRestController {

    /**
    *
    *
    * @ApiDoc(
    *  resource=true,
    *  description="Get all Roles, returns an array of json objects",
    *  statusCodes={
    *       200="Returned when data is present",
    *       404="Returned when no roles present"
    *    }
    * )
    * @Rest\Get("/role")
    */
    public function getAction( SerializerInterface $serializer )
    {
        $roles = $this->getDoctrine()
        ->getRepository( 'AppBundle:Role' )
        ->getAll();

        if ( !$roles ) {
            $view = new View( array( 'response' => 'No roles found' ), Response::HTTP_NOT_FOUND );
            $view->setTemplate( 'response.html.twig' );
            return $view;
        }

        $roles = $serializer->serialize( $roles, 'json' );
        $response = new Response( $roles );
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
    *
    * @ApiDoc(
    *  resource=true,
    *  description="Get a role by it's ID",
    *  statusCodes={
    *       200="Returned when data is present",
    *       404="Returned no role exists for that ID"
    *    },
    *   requirements={
    *       {
    *       "name"="roleId",
    *       "dataType"="integer",
    *       "description"="id of role to return",
    *       }
    *   }
    * )
    * @Rest\Get("/role/{roleId}")
    */
    public function getByIdAction( $roleId, SerializerInterface $serializer ) {
        $role = $this->getDoctrine()
        ->getRepository( 'AppBundle:Role' )
        ->get( $roleId );

        if ( !$role ) {
            $view = new View( array( 'response' => 'Role not found for ID ' .$roleId ), Response::HTTP_NOT_FOUND );
            $view->setTemplate( 'response.html.twig' );
            return $view;
        }

        $role = $serializer->serialize( $role, 'json' );
        $response = new Response( $role );
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
    *
    * @ApiDoc(
    *  resource=true,
    *  description="Get all users who have a certain role",
    *  statusCodes={
    *       200="Returned when data is present",
    *       404="Returned when an incorrect role id is passed"
    *    },
    *   requirements={
    *       {
    *       "name"="roleId",
    *       "dataType"="integer",
    *       "description"="id of role to use",
    *       }
    *   }
    * )
    * @Rest\Get("/members/{roleId}")
    */
    public function getAllMembersAction( $roleId, SerializerInterface $serializer ) {
        $role = $this->getDoctrine()
        ->getRepository( 'AppBundle:Role' )
        ->find( $roleId );

        if ( !$role ) {
            $view = new View( array( 'response' => 'Role not found for ID ' .$roleId ), Response::HTTP_NOT_FOUND );
            $view->setTemplate( 'response.html.twig' );
            return $view;
        }

        $members = $role->getMembers();

        $members = $serializer->serialize( $members, 'json' );
        $response = new Response( $members );
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}
