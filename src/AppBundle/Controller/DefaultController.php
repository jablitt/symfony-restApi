<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Product;
use AppBundle\Forms\UserType;
use AppBundle\Forms\OrderType;
use Doctrine\ORM\EntityManagerInterface;


class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/userForm")
     */
    public function CreateUserFormAction( ) {

        $form = $this->createForm( UserType::class, null, array(
            'action' => $this->generateUrl( 'createUserAction' ),
            'method' => 'POST'
        ) );

        return $this->render('form.html.twig', array(
           'form' => $form->createView()
        ));
    }



    /**
     * @Route("/orderForm")
     */
    public function orderFormAction() {

        $form = $this->createForm( OrderType::class, null, array(
            'action' => $this->generateUrl( 'createOrderAction' ),
            'method' => 'POST'
        ) );

        return $this->render('form.html.twig', array(
           'form' => $form->createView()
        ));
    }


}
