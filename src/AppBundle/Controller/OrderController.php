<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use FOS\RestBundle\View\View;
use AppBundle\Entity\Orders;

class OrderController extends FOSRestController {

    /**
    *
    *
    * @ApiDoc(
    *  resource=true,
    *  description="Get all Orders, returns an array of json objects",
    *  statusCodes={
    *       200="Returned when orders are present",
    *       404="Returned when no orders present"
    *    }
    * )
    * @Rest\Get("/order")
    */
    public function getAction( SerializerInterface $serializer )
    {
        $orders = $this->getDoctrine()
        ->getRepository( 'AppBundle:Orders' )
        ->findAll();

        if ( !$orders ) {
            $view = new View( array( 'response' => 'No orders found' ), Response::HTTP_NOT_FOUND );
            $view->setTemplate( 'response.html.twig' );
            return $view;
        }

        $orders = $serializer->serialize( $orders, 'json' );
        $response = new Response( $orders );
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
    *
    * @ApiDoc(
    *  resource=true,
    *  description="Get an Order, returns a json object",
    *  statusCodes={
    *       200="Returned when data is present",
    *       404="Returned when no order for that id"
    *    },
    *   requirements={
    *       {
    *       "name"="orderId",
    *       "dataType"="integer",
    *       "description"="id of the order to return",
    *       }
    *   }
    * )
    * @Rest\Get("/order/{orderId}")
    */
    public function getByIdAction( $orderId, SerializerInterface $serializer ) {
        $order = $this->getDoctrine()
        ->getRepository( 'AppBundle:Orders' )
        ->find( $orderId );

        if ( !$order ) {
            $view = new View( array( 'response' => 'Order not found for ID ' . $orderId ), Response::HTTP_NOT_FOUND );
            $view->setTemplate( 'response.html.twig' );
            return $view;
        }

        $order = $serializer->serialize( $order, 'json' );
        $response = new Response( $order );
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
    *
    * @ApiDoc(
    *  resource=true,
    *  description="Get all Orders by status, returns an array of json object",
    *  statusCodes={
    *       200="Returned when data is present",
    *       404="Returned when no orders for that status"
    *    },
    *   requirements={
    *       {
    *       "name"="status",
    *       "dataType"="any",
    *       "description"="order status",
    *       }
    *   }
    * )
    * @Rest\Get("/orderFilter/{status}")
    */
    public function getByStatusAction( $status, SerializerInterface $serializer ) {
        $orders = $this->getDoctrine()
        ->getRepository( 'AppBundle:Orders' )
        ->getByStatus( $status );

        if ( !$orders ) {
            $view = new View( array( 'response' => 'Orders not found for status ' . $status ), Response::HTTP_NOT_FOUND );
            $view->setTemplate( 'response.html.twig' );
            return $view;
        }

        $orders = $serializer->serialize( $orders, 'json' );
        $response = new Response( $orders );
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
    *
    *
    * @ApiDoc(
    *  resource=true,
    *  description="Create an Order, returns a success message",
    *  statusCodes={
    *       200="Returned when order created successfully",
    *    },
    * )
    * @Rest\Post("/order", name="createOrderAction")
    */
    public function createAction( Request $request ) {
        $order = new Orders();
        $requestData = $request->get( 'order' );

        //TODO check values
        $order->setStatus( $requestData[ 'status' ] );
        $order->setTotal( $requestData[ 'total' ] );

        $userId = $requestData[ 'userId' ];
        $user = $this->getDoctrine()
        ->getRepository( 'AppBundle:User' )
        ->find( $userId );

        $order->setUserId( $user );

        $retailerId = $requestData[ 'retailerId' ];
        $retailer = $this->getDoctrine()
        ->getRepository( 'AppBundle:Retailer' )
        ->find( $retailerId );

        $order->setRetailerId( $retailer );

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist( $order );
        $entityManager->flush();

        $view = new View( array( 'response' => 'Order added successfully' ), Response::HTTP_OK );
        $view->setTemplate( 'response.html.twig' );
        return $view;
    }

    /**
    *
    *
    * @ApiDoc(
    *  resource=true,
    *  description="Update an Order, returns an error or success message",
    *  statusCodes={
    *       200="Returned when order updated",
    *       404="Returned when no order for that id"
    *    },
    *   requirements={
    *       {
    *       "name"="orderId",
    *       "dataType"="integer",
    *       "description"="id of the order to return",
    *       }
    *   }
    * )
    * @Rest\Put("/order/{orderId}")
    */
    public function updateAction( $orderId, Request $request ) {
        $entityManager = $this->getDoctrine()->getManager();
        $requestData = $request->get( 'order' );

        $order = $this->getDoctrine()
        ->getRepository( 'AppBundle:Orders' )
        ->find( $orderId );

        if ( !$order ) {
            $view = new View( array( 'response' => 'Order not found for ID ' . $orderId ), Response::HTTP_NOT_FOUND );
            $view->setTemplate( 'response.html.twig' );
            return $view;
        }

        $order->setStatus( $requestData[ 'status' ] );
        $order->setTotal( $requestData[ 'total' ] );

        $userId = $requestData[ 'userId' ];
        $user = $this->getDoctrine()
        ->getRepository( 'AppBundle:User' )
        ->find( $userId );

        $order->setUserId( $user );

        $retailerId = $requestData[ 'retailerId' ];
        $retailer = $this->getDoctrine()
        ->getRepository( 'AppBundle:Retailer' )
        ->find( $retailerId );

        $order->setRetailerId( $retailer );

        $entityManager->persist( $order );
        $entityManager->flush();

        $view = new View( array( 'response' => 'Order updated successfully' ), Response::HTTP_OK );
        $view->setTemplate( 'response.html.twig' );
        return $view;
    }


}
