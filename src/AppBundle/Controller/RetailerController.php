<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use FOS\RestBundle\View\View;
use AppBundle\Entity\Retailer;

class RetailerController extends FOSRestController {

    /**
    *
    *
    * @ApiDoc(
    *  resource=true,
    *  description="Get all retailers, returns an array of json objects",
    *  statusCodes={
    *       200="Returned when data is present",
    *       404="Returned when no retailers present"
    *    }
    * )
    * @Rest\Get("/retailer")
    */
    public function getAction( SerializerInterface $serializer )
    {
        $retailers = $this->getDoctrine()
        ->getRepository( 'AppBundle:Retailer' )
        ->findAll();

        if ( !$retailers ) {
            $view = new View( array( 'response' => 'No retailers found' ), Response::HTTP_NOT_FOUND );
            $view->setTemplate( 'response.html.twig' );
            return $view;
        }

        $retailers = $serializer->serialize( $retailers, 'json' );
        $response = new Response( $retailers );
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
    *
    * @ApiDoc(
    *  resource=true,
    *  description="Get a retailer by it's ID",
    *  statusCodes={
    *       200="Returned when data is present",
    *       404="Returned when no retailer exists for that ID"
    *    },
    *   requirements={
    *       {
    *       "name"="retailerId",
    *       "dataType"="integer",
    *       "description"="id of retailer to fetch",
    *       }
    *   }
    * )
    * @Rest\Get("/retailer/{retailerId}")
    */
    public function getByIdAction( $retailerId, SerializerInterface $serializer ) {
        $retailer = $this->getDoctrine()
        ->getRepository( 'AppBundle:Retailer' )
        ->find( $retailerId );

        if ( !$retailer ) {
            $view = new View( array( 'response' => 'Retailer not found for ID ' . $retailerId ), Response::HTTP_NOT_FOUND );
            $view->setTemplate( 'response.html.twig' );
            return $view;
        }

        $retailer = $serializer->serialize( $retailer, 'json' );
        $response = new Response( $retailer );
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}
