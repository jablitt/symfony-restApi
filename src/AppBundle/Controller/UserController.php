<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use FOS\RestBundle\View\View;
use AppBundle\Entity\User;

class UserController extends FOSRestController {

    /**
    *
    *
    * @ApiDoc(
    *  resource=true,
    *  description="Get an all Users, returns an array of json objects",
    *  statusCodes={
    *       200="Returned when orders found",
    *       404="Returned when no orders in DB"
    *    }
    * )
    * @Rest\Get("/user")
    */
    public function getAction( SerializerInterface $serializer )
    {
        $users = $this->getDoctrine()
        ->getRepository( 'AppBundle:User' )
        ->getAll();

        if ( !$users ) {
            $view = new View( array( 'response' => 'No users found' ), Response::HTTP_NOT_FOUND );
            $view->setTemplate( 'response.html.twig' );
            return $view;
        }

        $users = $serializer->serialize( $users, 'json' );
        $response = new Response( $users );
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
    *
    *
    * @ApiDoc(
    *  resource=true,
    *  description="Get an Order by ID, also returns the users role",
    *  statusCodes={
    *       200="Returned when user found",
    *       404="Returned when no user for that id"
    *    },
    *   requirements={
    *       {
    *       "name"="userId",
    *       "dataType"="integer",
    *       "description"="id of the user to return",
    *       }
    *   }
    * )
    * @Rest\Get("/user/{userId}")
    */
    public function getByIdAction( $userId, SerializerInterface $serializer ) {
        $user = $this->getDoctrine()
        ->getRepository( 'AppBundle:User' )
        ->get( $userId );

        if ( !$user ) {
            $view = new View( array( 'response' => 'User not found for ID ' . $userId ), Response::HTTP_NOT_FOUND );
            $view->setTemplate( 'response.html.twig' );
            return $view;
        }

        $user = $serializer->serialize( $user, 'json' );
        $response = new Response( $user );
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
    *
    *
    * @ApiDoc(
    *  resource=true,
    *  description="Get all orders for a user by ID",
    *  statusCodes={
    *       200="Returned when orders found",
    *       404="Returned when no orders for that user, or invalid user id"
    *    },
    *   requirements={
    *       {
    *       "name"="userId",
    *       "dataType"="integer",
    *       "description"="id of the user to find orders for",
    *       }
    *   }
    * )
    * @Rest\Get("/userorders/{userId}")
    */
    public function getAllUserOrdersAction( $userId, SerializerInterface $serializer ) {
        $user = $this->getDoctrine()
        ->getRepository( 'AppBundle:User' )
        ->find( $userId );

        if ( !$user ) {
            $view = new View( array( 'response' => 'User not found for ID ' .$roleId ), Response::HTTP_NOT_FOUND );
            $view->setTemplate( 'response.html.twig' );
            return $view;
        }

        $orders = $user->getOrders();

        if ( !$orders ) {
            $view = new View( array( 'response' => 'Orders not found for ID ' .$roleId ), Response::HTTP_NOT_FOUND );
            $view->setTemplate( 'response.html.twig' );
            return $view;
        }

        $orders = $serializer->serialize( $orders, 'json' );
        $response = new Response( $orders );
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
    *
    *
    * @ApiDoc(
    *  resource=true,
    *  description="Create a new user",
    *  statusCodes={
    *       200="Returned when user created",
    *    }
    * )
    * @Rest\Post("/user", name="createUserAction")
    */
    public function createAction( Request $request, SerializerInterface $serializer ) {
        $user = new User();
        $requestData = $request->get( 'user' );

        //TODO check values
        $user->setFirstName(  $requestData[ 'firstname' ] );
        $user->setLastName( $requestData[ 'lastname' ] );
        $user->setEmail( $requestData[ 'email' ] );
        $user->setPassword( $requestData[ 'password' ] );

        $roleId = $requestData[ 'role' ];
        $role = $this->getDoctrine()
        ->getRepository( 'AppBundle:Role' )
        ->find( $roleId );

        $user->setRole( $role );

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist( $user );
        $entityManager->flush();

        $view = new View( array( 'response' => 'User created succesfully' ), Response::HTTP_OK );
        $view->setTemplate( 'response.html.twig' );
        return $view;
    }

    /**
    *
    *
    * @ApiDoc(
    *  resource=true,
    *  description="Update a user",
    *  statusCodes={
    *       200="Returned when user updated",
    *       404="Returned when invalid id passed"
    *    },
    *   requirements={
    *       {
    *       "name"="userId",
    *       "dataType"="integer",
    *       "description"="id of the user to update",
    *       }
    *   }
    * )
    * @Rest\Put("/user/{userId}", name="updateUserAction")
    */
    public function updateAction( $userId, Request $request ) {
        $entityManager = $this->getDoctrine()->getManager();
        $requestData = $request->get( 'user' );

        $user = $this->getDoctrine()
        ->getRepository( 'AppBundle:User' )
        ->find( $userId );

        if ( !$user ) {
            $view = new View( array( 'response' => 'User not found for ID ' . $userId ), Response::HTTP_NOT_FOUND );
            $view->setTemplate( 'response.html.twig' );
            return $view;
        }

        $user->setFirstName( $requestData[ 'firstname' ] );
        $user->setLastName( $requestData[ 'lastname' ] );
        $user->setEmail( $requestData[ 'email' ] );
        $user->setPassword( $requestData[ 'password' ] );

        $roleId = $requestData[ 'role' ];
        $role = $this->getDoctrine()
        ->getRepository( 'AppBundle:Role' )
        ->find( $roleId );

        $user->setRole( $role );

        $entityManager->persist( $user );
        $entityManager->flush();

        $view = new View( array( 'response' => 'User updated succesfully' ), Response::HTTP_OK );
        $view->setTemplate( 'response.html.twig' );
        return $view;
    }


}
