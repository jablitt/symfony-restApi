<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Orders;
use AppBundle\Entity\Retailer;
use AppBundle\Entity\User;
use AppBundle\Entity\Role;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class Fixtures extends Fixture
{

    private $roleNames = [ 'guest', 'user', 'vip' ];
    private $userFirstNames = [ 'josh', 'james', 'john' ];
    private $userLastNames = [ 'second', 'surname', 'last' ];
    private $passwords = [ 'secret', 'test', 'fake' ];
    private $locations = [ 'leeds', 'london', 'manchester'];
    private $names = [ 'coop', 'asda', 'tesco' ];
    private $status = [ 'shipped', 'shipped', 'packed' ];


    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 3; $i++) {
            $role = new Role();
            $role->setName( $this->roleNames[$i] );
            $manager->persist( $role );
            $user = new User();
            $user->setFirstName( $this->userFirstNames[$i] );
            $user->setLastName( $this->userLastNames[$i] );
            $user->setEmail( $this->userFirstNames[$i] . '@' . $this->userLastNames[$i] );
            $user->setPassword( $this->passwords[$i] );
            $user->setRole( $role );
            $manager->persist( $user );
            $retailer = new Retailer();
            $retailer->setName( $this->locations[$i] );
            $retailer->setLocation( $this->names[$i] );
            $retailer->setEmail( $this->userFirstNames[$i] . '@' . $this->userLastNames[$i] );
            $retailer->setSecret( $this->passwords[$i] );
            $manager->persist( $retailer );
            $order = new Orders();
            $order->setUserId( $user );
            $order->setRetailerId( $retailer );
            $order->setStatus( $this->status[$i] );
            $order->setTotal( $i );
            $manager->persist( $order );
        }

        $manager->flush();
    }
}
